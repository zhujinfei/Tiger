/**
 * 
 */
package cn.zhucongqi.tiger.db.models;

import com.jfinal.plugin.activerecord.Record;

/**
 * 记录数据
 * @author BruceZCQ [zcq@zhucongqi.cn]
 * @version
 */
public class TigerRecord extends Record {

	private static final long serialVersionUID = -4370318280946200203L;
	
	public TigerRecord(){
		this.update();
	}
	
	public void update(){
		this.set("id","0");
	}
}
