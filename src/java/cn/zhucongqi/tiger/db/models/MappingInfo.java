package cn.zhucongqi.tiger.db.models;

import cn.zhucongqi.tiger.consts.Consts;

import com.jfinal.plugin.activerecord.Model;

/**
 * app与数据表映射关系
 * @author BruceZCQ [zcq@zhucongqi.cn]
 * @version
 */
public class MappingInfo extends Model<MappingInfo> {
	
	private static final long serialVersionUID = 766500888165937962L;
	public static final String table = Consts.TABLE_NAME_PREFIX+"mapping_info";
	public static final MappingInfo dao = new MappingInfo();
	
}
