package cn.zhucongqi.tiger.db.models;

import cn.zhucongqi.tiger.consts.Consts;

import com.jfinal.plugin.activerecord.Model;

/**
 * 客户端授权校验
 * @author BruceZCQ [zcq@zhucongqi.cn]
 * @version
 */
public class AppSecretKeys extends Model<AppSecretKeys> {

	private static final long serialVersionUID = 420885798364511489L;
	public static final String table = Consts.TABLE_NAME_PREFIX+"app_secret_keys";
	public static final AppSecretKeys dao = new AppSecretKeys();

}
