package cn.zhucongqi.tiger.db.models;

import cn.zhucongqi.tiger.consts.Consts;

import com.jfinal.plugin.activerecord.Model;

/**
 * 设备token
 * @author BruceZCQ [zcq@zhucongqi.cn]
 * @version
 */
public class DeviceToken extends Model<DeviceToken> {
	
	private static final long serialVersionUID = 420885798364511489L;
	public static final String table = Consts.TABLE_NAME_PREFIX+"device_token";
	public static final DeviceToken dao = new DeviceToken();
	
}
