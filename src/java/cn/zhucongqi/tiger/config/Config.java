/**
 * JFinal 的配置信息
 */
package cn.zhucongqi.tiger.config;

import cn.zhucongqi.tiger.consts.Consts;
import cn.zhucongqi.tiger.controllers.DashboardController;
import cn.zhucongqi.tiger.data.DataKit;
import cn.zhucongqi.tiger.db.models.AppSecretKeys;
import cn.zhucongqi.tiger.db.models.DeviceToken;
import cn.zhucongqi.tiger.db.models.MappingInfo;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext2.config.JFinalConfigExt;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

/**
 * config
 * @author BruceZCQ [zcq@zhucongqi.cn]
 * @version
 */
public class Config extends JFinalConfigExt {

	@Override
	public void configMoreConstants(Constants me) {
		
		if (StrKit.isBlank(Consts.TABLE_NAME_PREFIX)) {
			throw new RuntimeException("Please set the table name prefix first!");
		}
	}

	@Override
	public void configMoreRoutes(Routes me) {
		 //指定/controller
		 me.add("/", DashboardController.class);		
	}

	@Override
	public void configMorePlugins(Plugins me) {
		
	}

	@Override
	public void configTablesMapping(ActiveRecordPlugin arp) {
		arp.addMapping(MappingInfo.table, MappingInfo.class);
		arp.addMapping(AppSecretKeys.table, AppSecretKeys.class);
		arp.addMapping(DeviceToken.table, DeviceToken.class);
	}

	@Override
	public void configMoreInterceptors(Interceptors me) {
		
	}

	@Override
	public void configMoreHandlers(Handlers me) {
		
	}

	@Override
	public void afterJFinalStarted() {
		//初始化数据
		DataKit.init();		
	}

}
