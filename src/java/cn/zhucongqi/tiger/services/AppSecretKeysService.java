package cn.zhucongqi.tiger.services;

import java.util.List;

import cn.zhucongqi.tiger.consts.Consts;
import cn.zhucongqi.tiger.db.models.AppSecretKeys;
import cn.zhucongqi.tiger.db.models.DeviceToken;

import com.jfinal.ext2.core.Service;
import com.jfinal.ext2.kit.DateTimeKit;
import com.jfinal.ext2.kit.RandomKit;
import com.jfinal.kit.HashKit;

/**
 * 处理移动客户端请求
 * @author BruceZCQ [zcq@zhucongqi.cn]
 * @version
 */
public class AppSecretKeysService extends Service {

	/**
	 * 授权
	 * @param uuid
	 * @param appkey
	 * @param bundle
	 * @return
	 */
	public void authDevice(String uuid, String appkey, String bundle){
		List<AppSecretKeys> appSecretKeys = AppSecretKeys.dao.find("SELECT * FROM " + AppSecretKeys.table + " WHERE bundle = ? "
				+ "AND appkey = ? "
				+ "AND secretkey=md5(concat(secretkeysalt,bundle,"
				+ "appkey,secretkeysalt,appkey,bundle,secretkeysalt)) LIMIT 1 ", bundle, appkey);

		DeviceToken deviceToken= new DeviceToken();
		String token = null;
		Long timeout = null;
		
		if (null != appSecretKeys && appSecretKeys.size() == 1) {
			token = RandomKit.randomMD5Str();
			timeout = DateTimeKit.getUnixTimeAfterDay(Consts.TOKEN_TIMEOUT_DAYS);
			deviceToken.set("uuid", uuid)
			.set("token", token)
			.set("timeout", timeout);
			if (deviceToken.save()) {
				this.controller.setAttr("token", token);
				this.controller.setAttr("timeout", timeout);
				this.controller.renderJson();
			}
		}else {
			this.controller.renderError(401);
		}
 	}
		
	/**
	 * 首次注册app
	 * @param packagevalue
	 */
	public void addApp(String packagevalue) {
			packagevalue = this.wrapPackagename(packagevalue);
			String appKey = HashKit.md5(RandomKit.randomStr());
			String secretkeysalt = HashKit.md5(RandomKit.randomStr());
			String bundle = HashKit.md5(packagevalue);
			String secretkey = HashKit.md5(secretkeysalt + bundle + appKey
					+ secretkeysalt + appKey + bundle + secretkeysalt);
			AppSecretKeys appSecretKeys = new AppSecretKeys();
			appSecretKeys.set("id", 0).set("bundle", bundle)
					.set("appkey", appKey).set("secretkey", secretkey)
					.set("package", packagevalue)
					.set("secretkeysalt", secretkeysalt);
			if (appSecretKeys.save()) {
				this.controller.renderText("包名称:<br/>" + packagevalue + "<br/>AppKey:<br/>" + appKey
						+ "<br/>请把此key拷贝到客户端使用");
			} else {
				// TODO 自定义错误码
				this.controller.renderText("ec:500");
			}
	}
	
	/**
	 * 校验包名称是否已被注册。
	 * @param packagevalue
	 */
	public void validatePackage(String packagevalue){
		packagevalue = this.wrapPackagename(packagevalue);
		List<AppSecretKeys> appSecretKeys = AppSecretKeys.dao.find(
				"SELECT id FROM " + AppSecretKeys.table
						+ " WHERE package = ? LIMIT 1", packagevalue);
		if (appSecretKeys.size() != 0) {
			this.controller.renderText("包名称:<br/>" + packagevalue + "已经存在,请重新输入！");
		}else{
			this.controller.renderText("包名称:<br/>" + packagevalue + "可以使用！");
		}
	}
	
	/**
	 * packagename warp
	 * @param packageValue
	 * @return
	 * TODO 包名处理
	 */
	private String wrapPackagename(String packageValue){
		String packagename = Consts.DEFAULT_APP_PACKAGE_NAME;
		if (packageValue.startsWith(packagename)) {
			return packageValue;
		}
		return packagename + packageValue;
	}
}
