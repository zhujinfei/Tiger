/**
 *  ViewPath ，所以页面对应的目录
 */
package cn.zhucongqi.tiger.consts;

/**
 * 
 * @author BruceZCQ [zcq@zhucongqi.cn]
 * @version
 */
public class ViewPaths {

	// user 的 dir
	public static final String USER_VIEW_PATH = "user";
	
	// code
	public static final String CODE_VIEW_PATH = "code";
	
	// app
	public static final String APP_VIEW_PATH = "app";
	
	// dashboard
	public static final String DASHBOARD_VIEW_PATH = "dashboard";
}
