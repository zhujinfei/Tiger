package cn.zhucongqi.tiger.controllers;

import java.util.ArrayList;
import java.util.List;

import cn.zhucongqi.tiger.consts.Consts;
import cn.zhucongqi.tiger.consts.ViewPaths;
import cn.zhucongqi.tiger.data.DataKit;
import cn.zhucongqi.tiger.models.Platform;
import cn.zhucongqi.tiger.models.TableMaker;
import cn.zhucongqi.tiger.models.TableMaker.TableType;
import cn.zhucongqi.tiger.services.AppSecretKeysService;
import cn.zhucongqi.tiger.services.TableMakerService;
import cn.zhucongqi.tiger.validators.TigerTableMakerValidator;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.jfinal.aop.Before;
import com.jfinal.ext2.core.ControllerExt;
import com.jfinal.ext2.kit.DDLKit.Column;
import com.jfinal.ext2.kit.PageViewKit;

/**
 * 
 * @author BruceZCQ [zcq@zhucongqi.cn]
 * @version
 */
public class TigerController extends ControllerExt {

	private AppSecretKeysService appSecretKeysService;
	private TableMakerService tableMakerService;

	public void index(){
		this.appV();
	}

	/**
	 * 添加 Appview
	 */
	private void appV(){
		this.setAttr("packname", Consts.DEFAULT_APP_PACKAGE_NAME);
		this.render(PageViewKit.getJSPPageViewFromWebInf(ViewPaths.APP_VIEW_PATH, "index"));
	}
	
	/**
	 * 预览 DDL
	 */
	@Before({TigerTableMakerValidator.class})
	public void preview(){
		TableMaker tableMaker = this.getTableMaker();
		if (null == tableMaker) {
			this.renderText("参数错误");
		}else{
			this.renderText(tableMaker.ddl(TableType.DATA, Platform.IOS_10));	
		}
	}
	
	/**
	 * 获取 maker model
	 * @return
	 */
	private TableMaker getTableMaker(){
		
		String appId = this.getPara("app");
		String code = this.getPara("code").trim();
		String codeComment = this.getPara("codecomment").trim();

		JSONArray fields = null;
		try {
			fields = JSON.parseArray(this.getPara("fields"));
		} finally{
			if (null == fields) {
				return null;
			}
		}
		
		List<Column> columns = new ArrayList<Column>();
		for (int i = 0; i < fields.size(); i++) {
			String[] field = fields.getString(i).split(":");
			if (field.length != 7) {
				return null;
			}
			String colName = field[0];
			int colType = Integer.valueOf(field[1]);
			int colSize = Integer.valueOf(field[2]);
			String colInitVal = field[3];
			String colComment = field[4];
			boolean pk = Integer.valueOf(field[5]) == 0 ? false : true;
			boolean uk = Integer.valueOf(field[6]) == 0 ? false : true;
			Column column = new Column(colName, DataKit.getTypeName(colType), colComment, colSize, colInitVal, pk, uk);
			columns.add(column);
		}
		
		TableMaker tableMaker = new TableMaker();
		tableMaker.setAppId(appId);
		tableMaker.setCode(code);
		tableMaker.setIntro(codeComment);
		tableMaker.setColumns(columns);
		return tableMaker;
	}
	
	/**
	 * 添加一个App
	 */
	public void addApp(){
		String packagevalue = this.getPara("packagename");
		appSecretKeysService.addApp(packagevalue);
	}

	/**
	 * 添加 code
	 */
	public void regCodeV(){
		this.apps();
		this.tableColumnTypes();
		this.render(PageViewKit.getJSPPageViewFromWebInf(ViewPaths.CODE_VIEW_PATH,"index"));
	}
	
	private void apps(){
		this.setAttr("apps", tableMakerService.getAllApps());
	}
	
	private void tableColumnTypes(){
		this.setAttr("types", DataKit.getColumnTypes());
	}
	
	/**
	 * 注册code  创建相应表
	 */
	@Before({TigerTableMakerValidator.class})
	public void regCode() {
		TableMaker tableMaker = this.getTableMaker();
		if (null == tableMaker) {
			this.renderText("参数错误");
		}else{
			tableMakerService.regCode(tableMaker);
		}
	}
	
	/**
	 * 校验包名是否可用
	 */
	public void validatePackage() {
		String packagevalue = this.getPara("packagename");
		appSecretKeysService.validatePackage(packagevalue);
	}
}
